# Contrib

A timeline of opensource contributions. Takes in your Gitlab and/or Github username(s) and lists all public commits.

## Possible Features

- Generate colors for each project
  
  - Color shades are grouped by project owner

## API Tokens

After a quick glance, reading the necessary data from the API of the respective service will require authentication. I believe the minimal permissions needed are the following:  

Gitlab - `read_user`

Github - `repo > public_repo` & `user > read:user`


